<?php 
    require('Models/Category.php');
    class CategoryController {
        var $model;
        function __construct()
        {
           $this->model = new Category();
        }
       public function add() {
          require('Views/Category/add.php');
       }
       public function list() {
         $categories = $this->model->all();
        require('Views/Category/list.php');
    }
    public function store() {
        $data = $_POST;
        $status = $this->model->create($data);
        if($status=1) {
            echo "Thêm Mới Thành Công";
        }
        else {
            echo "Thêm Mới Thất Bại";
        }
    }

    public function delete() {
        $id = $_GET['id'];
        $status = $this->model->delete($id);
        if($status=1) {
            echo "Xóa Thành Công";
        }
        else {
            echo "Xóa Thất Bại";
        }
        
    }
    
    }
$controller = new CategoryController();
?>