<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Zent Education</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <h2 align="center">DANH SÁCH BÀI VIẾT </h2>
        <a href="product_add.php" class="btn btn-primary">Thêm mới</a>
        <table class="table">
            <thead>
              <tr>
                <th>Mã Bài Viết</th>
                <th width="300">Tên Bài Viêt</th>
                <th width="300">Miêu Tả</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($posts as $row) {?>
              <tr>
                <td><?= $row['Ma_Bai_viet'] ?></td>
                <td><?= $row['Tieu_De'] ?></td>
                <td><?= $row['Mieu_Ta'] ?></td>
                <td>
                    <a href="index.php?mod=product&act=detail&MA_SP=<?= $row['MA_SP'] ?>" class="btn btn-success">Detail</a> 
                     <a href="product_edit.php?MA_SP=<?= $row['MA_SP'] ?>" class="btn btn-warning">Update</a>  
                    <a href="product_delete.php?MA_SP=<?= $row['MA_SP'] ?>" class="btn btn-danger">Delete</a>

                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
    </div>
</body>
</html>