<?php 
session_start();
include("sanpham.php");
$idProduct = $_GET['masp'];
$newProduct = array();
foreach( $Product as $value) {
    $newProduct[$value['masp']] = $value;
}

if(!isset($_SESSION['cart']) || $_SESSION['cart'] == null)   {
   $newProduct[$idProduct]['qty'] = 1 ;
   $_SESSION['cart'][$idProduct] = $newProduct[$idProduct];
} 
else {

   if(array_key_exists($idProduct , $_SESSION['cart'])) {
     $_SESSION['cart'][$idProduct]['qty'] += 1;
   }
   else {
       $newProduct[$idProduct]['qty'] = 1;
       $_SESSION['cart'][$idProduct] = $newProduct[$idProduct];
   }
}

header("location:cart.php");

?>