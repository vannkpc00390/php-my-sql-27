 <?php  
   require("menu.php");
   require("header.php");
   require("connet.php");
    // Câu lệnh truy vấn
    $query = " SELECT * FROM bai_viet";

    // Thực thi câu lệnh
            $result = $conn->query($query);
    // Tạo 1 mảng để chứa dữ liệu
            $posts = array();
    
            while($row = $result->fetch_assoc()) { 
                $post[] = $row;
            }
 ?>

  <!-- Begin Page Content -->
  <div class="container-fluid">

<!-- Page Heading -->
       <div class="d-sm-flex align-items-center justify-content-between mb-4">
       <div class="container">
     <h3 class="text-center">--- Quản Lý Bài Viết ---</h3>
     <a href="form_add_post.php" class="btn btn-primary mb-3">Thêm Bài Viết</a>
     <table class="table">
         <thead class="text-center">
            <tr>
                <th width="120">Mã Bài Viết</th>
                <th>Tên</th>
                <th>Description</th>
                <th width="200">Hành Động</th>
            </tr>
         </thead>
         <?php foreach($post as $item) {?>
         <tr class="text-center">
             <td><?= $item['Ma_Bai_viet']; ?></td>
             <td><?= $item['Tieu_De']; ?></td>
             <td><?= $item['Mieu_Ta']; ?></td>
             <td>
                 <a href="form_edit_post.php?id=<?= $item['Ma_Bai_viet'];?>" class="btn btn-success">Edit</a>
                 <a href="delete_post.php?id=<?= $item['Ma_Bai_viet'];?>" class="btn btn-danger">Delete</a>
             </td>
         </tr>
         <?php } ?>
     </table>
 </div>
       </div>
   </div>
  <?php 
    require("footer.php")
 ?>