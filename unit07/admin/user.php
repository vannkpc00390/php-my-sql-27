 <?php  
   require("menu.php");
   require("header.php");
 ?>

 <!-- Begin Page Content -->
      <div class="container-fluid">

   <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <div class="container">
        <h3 class="text-center">--- Quản Lý Người Dùng ---</h3>
        <a href="#" class="btn btn-primary mb-3">Thêm Người Dùng</a>
        <table class="table">
            <thead>
                <th>Mã Người Đăng</th>
                <th>Tên Người Đăng</th>
                <th>Ảnh </th>
                <th>Email</th>
                <th>Hành Động</th>
            </thead>
            <tr>
                <td>1</td>
                <td>Thời sự</td>
                <td>
                    <img src="https://video-thumbs.mediacdn.vn//vtv/2018/10/2/0210thoi-su-19h-15384852850441347953968-a1b84_thumb3.jpg" width="100px" height="100px">
                </td>
                <td>Tin về thời sự</td>
                <td>
                    <a href="#" class="btn btn-primary">Detail</a>
                    <a href="#" class="btn btn-success">Edit</a>
                    <a href="#" class="btn btn-danger">Delete</a>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Bóng đá</td>
                <td>
                    <img src="https://image.thanhnien.vn/660/uploaded/quangtuyen/2019_03_05/u23tuanlinh_fpjh.jpg" width="100px" height="100px">
                </td>
                <td>Tin về bóng đá</td>
                <td>
                    <a href="#" class="btn btn-primary">Detail</a>
                    <a href="#" class="btn btn-success">Edit</a>
                    <a href="#" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        </table>
    </div>
          </div>
      </div>
  <?php 
    require("footer.php")
 ?>