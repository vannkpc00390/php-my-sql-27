<?php  
   require("menu.php");
   require("header.php");
   require("connet.php");
 // Câu lệnh truy vấn
 $query = "SELECT * FROM the_loai";

 // Thực thi câu lệnh
         $result = $conn->query($query);
 // Tạo 1 mảng để chứa dữ liệu
         $categories = array();
 
         while($row = $result->fetch_assoc()) { 
             $categories[] = $row;
         }
 ?>

  <!-- Begin Page Content -->
  <div class="container-fluid">

<!-- Page Heading -->
       <div class="d-sm-flex align-items-center justify-content-between mb-4">
       <div class="container">
     <h3 class="text-center">--- Quản Lý Thể Loại ---</h3>
     <a href="#" class="btn btn-primary mb-3">Thêm Thể Loại</a>
     <table class="table">
            <thead>
                <th>Mã Thể Loại</th>
                <th>Tên</th>
                <th>Ảnh</th>
                <th>Miêu Tả</th>
                <th>Hành Động</th>
            </thead>
            <?php foreach($categories as $item) {?>
            <tr>
                <td><?= $item['Ma_The_loai'] ?></td>
                <td><?= $item['Ten'] ?></td>
                <td>
                    <img src="><?= $item['anh'] ?>" width="100px" height="100px">
                </td>
                <td><?= $item['Tom_Tat'] ?></td>
                <td>
                    <a href="#" class="btn btn-success">Edit</a>
                    <a href="#" class="btn btn-danger">Delete</a>
                </td>
            </tr>
                <?php }?>
            
        </table>
 </div>
       </div>
   </div>
  <?php 
    require("footer.php")
 ?>