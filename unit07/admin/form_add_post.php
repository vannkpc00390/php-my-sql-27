<?php  
   require("menu.php");
   require("header.php");
 ?>

  <!-- Begin Page Content -->
  <div class="container-fluid">

<!-- Page Heading -->
       <div class="d-sm-flex align-items-center justify-content-between mb-4">
       <div class="container">
        <h3 class="text-center">Thêm Bài Viết Mới</h3>
        <div class="container">
        <form action="add_post.php" method="POST" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label for="">Tên Bài Viết</label>
                <input type="text" class="form-control" id="" placeholder="" name="name">
            </div>
            <div class="form-group">
                <label for="">Description</label>
                <input type="text" class="form-control" id="" placeholder="" name="Description">
            </div>
            <div class="form-group">
                <label for="">Nội Dung</label>
                <textarea id="summernote" name="summernote"></textarea>
                <script>
                    $('#summernote').summernote({
                        placeholder: 'Mời Bạn Nhập Nội Dung',
                        tabsize: 2,
                        height: 100
                     });
                  </script>
            </div>
            <button type="submit" name="submit" class="btn btn-primary">Thêm</button>
        </form>
    </div>
  <?php 
    require("footer.php")
 ?>