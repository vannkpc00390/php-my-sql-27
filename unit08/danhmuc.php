<?php
    class danhmuc {
        private $tieude;
        private $duongdan;
        private $danhmuccha;
        private $anh;
        private $mota;

          // tiêu đề
          public function settieude($tieude) {
            $this->tieude = $tieude;
        } 
        public function gettieude() {
            return $this-> tieude;
        }
        // đường đẫn
        public function setduongdan($duongdan) {
            $this->duongdan = $duongdan;
        }
        public function getduongdan() {
            return $this->duongdan ; 
        }
    
        // danh mục cha
        public function setdanhmuccha($danhmuccha) {
            $this->danhmuccha = $danhmuccha;
        }
        public function getdanhmuccha() {
            return $this->danhmuccha;
        }

        // ảnh hiển thị
        public function setanh($anh) {
            $this->anh = $anh;
        }
        public function getanh() {
            return $this->anh;
        }
        
         // mô tả
        public function setmota($mota) {
            $this->mota = $mota;
        }
        public function getmota() {
            return $this->mota ; 
        }
    }
    $danhmuc = new danhmuc();
    $danhmuc->settieude('Kim Min Kyu (Produce X 101) - Jooyeon (The Boyz) xét nghiệm COVID-19 chỉ vì 1 nhóm idol nữ');
    $danhmuc->setduongdan('https://saostar.vn/giai-tri/kim-min-kyu-produce-x-101-jooyeon-the-boyz-xet-nghiem-covid-19-7285787.html');
    $danhmuc->setdanhmuccha('Giải Trí');
    $danhmuc->setanh('https://ss-images.saostar.vn/wp1000/2020/04/04/7285471/a8430436-f5a9-4d2d-b3b9-b93f269e48d5.jpeg');
    $danhmuc->setmota('Làm người dẫn chương trình với một nữ idol có nhân viên nhiễm Covid-19, Kim Min Kyu và Jooyeon buộc cách ly và xét nghiệm y tế!');
?>