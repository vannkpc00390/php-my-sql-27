<?php
    class nguoidung {
        private $ten ;
        private $email;
        private $matkhau;
        private $anh;
        // tên người dùng
        public function setten($ten) {
            $this->ten = $ten;
        }
        public function getten() {
            return $this->ten;
        } 
        // email người dùng
        public function setemail($email) {
            $this->email = $email;
        } 
        public function getemail() {
            return $this->email;
        }
        // mật khẩu
        public function setmatkhau($matkhau) {
            $this->matkhau = $matkhau;
        }
        public function getmatkhau() {
            return $this->matkhau;
        }
        // ảnh người dùng
        public function setanh($anh) {
            $this->anh = $anh;
        }
        public function getanh() {
            return $this->anh;
        }
    }
$nguoidung = new nguoidung;
$nguoidung->setten('Nguyễn Khánh Văn');
$nguoidung->setemail('Vannkpc00390@gmail.com');
$nguoidung->setmatkhau('123');
$nguoidung->setanh('https://ss-images.saostar.vn/wp1000/2020/04/04/7285787/he0yj8p.jpg');
?>