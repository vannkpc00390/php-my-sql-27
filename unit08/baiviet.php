<?php 
    class baiviet {
        private $tieude;
        private $duongdan;
        private $mota;
        private $noidung;
        // tiêu đề
        public function settieude($tieude) {
            $this->tieude = $tieude;
        } 
        public function gettieude() {
            return $this-> tieude;
        }
        // đường đẫn
        public function setduongdan($duongdan) {
            $this->duongdan = $duongdan;
        }
        public function getduongdan() {
            return $this->duongdan ; 
        }
        // mô tả
        public function setmota($mota) {
            $this->mota = $mota;
        }
        public function getmota() {
            return $this->mota ; 
        }
        // nội dung
        public function setnoidung($noidung) {
            $this->noidung = $noidung;
        }
        public function getnoidung() {
            return $this->noidung;
        }
    }
    $baiviet = new baiviet();
     // khởi tạo phương thức
     $baiviet->settieude('Kim Min Kyu (Produce X 101) - Jooyeon (The Boyz) xét nghiệm COVID-19 chỉ vì 1 nhóm idol nữ');
     $baiviet->setduongdan('https://saostar.vn/giai-tri/kim-min-kyu-produce-x-101-jooyeon-the-boyz-xet-nghiem-covid-19-7285787.html');
     $baiviet->setmota('Làm người dẫn chương trình với một nữ idol có nhân viên nhiễm Covid-19, Kim Min Kyu và Jooyeon buộc cách ly và xét nghiệm y tế!');
     $baiviet->setnoidung('Theo đó, đại diện công ty quản lý của nam ngôi sao trẻ tiết lộ: “Kim Min Kyu đã trải qua thử nghiệm COVID19. 
     Nó như một biện pháp phòng ngừa bổ sung, một số nhân viên của cậu ấy cũng đã xét nghiệm. 
     Kim Min Kyu hiện đang tự cách ly ở nhà trong khi chờ kết quả xét nghiệm của mình. 
     Kết quả COVID19 đầy đủ của Kim Min Kyu sẽ được công bố vào thứ Hai tuần sau (tức ngày 06/04)” - Jellyfish Entertainment chia sẻ.');

?>