<?php 
    require("Kiemtra.php")
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Form</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <form action="xuly.php" method="post" role="form">
            <legend>Thực Hành Gởi Dữ Liệu Từ Post</legend>
            
            <div class="form-group">
                <label for="">Mã Sinh Viên</label>
                <input type="text" class="form-control" id="" placeholder="Mã Sinh Viên" name="masv">
            </div>
            
            <div class="form-group">
                <label for="">Họ Và Tên</label>
                <input type="text" class="form-control" id="" placeholder="Họ Và Tên" name="ten">
            </div> 
            <div class="form-group">
                <label for="">Số Điện Thoại</label>
                <input type="text" class="form-control" id="" placeholder="Số Điện Thoại" name="sdt">
            </div>   
            <div class="form-group">
                <label for="">Email</label>
                <input type="text" class="form-control" id="" placeholder="Email" name="email">
            </div>  
            <div class="form-group">
                <label for="">Phái</label>
                <input type="text" class="form-control" id="" placeholder="Phái" name="phai">
            </div>
            <div class="form-group">
                <label for="">địa chỉ</label>
                <input type="text" class="form-control" id="" placeholder="Địa Chỉ" name="diachi">
            </div>
            <button type="submit" class="btn btn-primary">Gửi Thông Tin</button>
        </form>
    </div>
</body>
</html>