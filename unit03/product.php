<?php
session_start();
   require("list.php");
   $total = 0;
   if(!isset($_SESSION['cart']) || $_SESSION['cart']) {
    foreach($_SESSION['cart'] as $tong) {
        $total += $tong['qty'];
    }
} 
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
  Đang Có <a href='cart.php'><?= $total; ?></a> Sản Phẩm Trong Giỏ Hàng
      <div class="container">
        <table class="table" style="text-align: center;">   
            <thead>
                <tr>
                <th scope="col">ID</th>
                <th scope="col">Tên Sản Phẩm</th>
                <th scope="col">Giá</th>
                <th scope="col">Hành Động</th>
                </tr>
            </thead>
            <tbody>
                <?php
                 foreach($product as $products) {?>
                    <tr>
                        <th scope="row"><?= $products['id'] ; ?></th>
                        <td><?= $products['tensp'] ; ?></td>
                        <td><?= number_format($products['gia'])  ; ?></td>
                        <td><a href="insertcard.php?id=<?= $products['id']; ?>" style="text-decoration: none; color:blue; ">Thêm Vào Giỏ Hàng</a></td>
                    </tr>
            </tbody>
            <?php }?>
        </table>
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>